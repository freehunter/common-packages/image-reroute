function convertObjectToQueryString(obj) {
    var str = [];
    for (var p in obj) 
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");
}
function imageReroute(imageURL, options) {
    let imageArguments = Object.assign(
        {
            quality: 70,
        },
        options
    );
    if(!imageURL) throw new Error('No imageURL is being specify, so I have no idea which image should I resize, I am so very lost right now, plz send help');
    if(typeof imageURL !== 'string') throw new Error(`Argument "imageURL" is NOT typeof string, I need string, but u give me ${typeof imageURL}, y? I do not get, plz send help`);
    let baseURL = 'http://localhost:6219?url=';
    if(process.env.ENV === 'production') baseURL = 'https://images.freehunter.io?url=';
    const imageArgString = convertObjectToQueryString(imageArguments).toString();
    return `${baseURL}${imageURL}&${imageArgString}`;
}
module.exports = imageReroute;